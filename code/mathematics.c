#include "mathematics.h"
#include "common.h"
#include "debug.h"
#include <math.h>

Vec2 vec2(float x, float y)
{
    return (Vec2) {x, y};
}

Vec2 vec2_negate(Vec2 v)
{
    return (Vec2) {-v.x, -v.y};
}

Vec2 vec2_add(Vec2 a, Vec2 b)
{
    return (Vec2) {a.x + b.x, a.y + b.y};
}

Vec2 vec2_sub(Vec2 a, Vec2 b)
{
    return vec2_add(a, vec2_negate(b));
}

Vec2 vec2_mult(Vec2 a, Vec2 b)
{
    return (Vec2) {a.x * b.x, a.y * b.y};
}

Vec2 vec2_multf(Vec2 a, float b)
{
    return (Vec2) {a.x * b, a.y * b};
}

Vec2 vec2_div(Vec2 a, Vec2 b)
{
    return (Vec2) {a.x / b.x, a.y / b.y};
}

Vec2 vec2_divf(Vec2 a, float b)
{
    return vec2_multf(a, 1.f / b);
}

float vec2_dot(Vec2 a, Vec2 b)
{
    return (a.x * b.x + a.y * b.y);
}

bool vec2_equals(Vec2 a, Vec2 b)
{
    return (a.x == b.x && a.y == b.y);
}

float vec2_lengthsq(Vec2 v)
{
    return (v.x * v.x + v.y * v.y);
}

float vec2_length(Vec2 v)
{
    return sqrtf(vec2_lengthsq(v));
}

Vec2 vec2_normalize(Vec2 v)
{
    Vec2 result = {0};

    float length = vec2_length(v);

    if (length != 0.f)
    {
        result = (Vec2) {
            .x = v.x * (1.f / length),
            .y = v.y * (1.f / length)
        };
    }

    return result;
}

Vec3 vec3(float x, float y, float z)
{
    return (Vec3) {x, y, z};
}

Vec2 vec3_xy(Vec3 v)
{
    return (Vec2) {v.x, v.y};
}

Vec2 vec3_xz(Vec3 v)
{
    return (Vec2) {v.x, v.z};
}

Vec2 vec3_yz(Vec3 v)
{
    return (Vec2) {v.y, v.z};
}

Vec3 vec3_negate(Vec3 v)
{
    return (Vec3) {-v.x, -v.y, -v.z};
}

Vec3 vec3_add(Vec3 a, Vec3 b)
{
    return (Vec3) {a.x + b.x, a.y + b.y, a.z + b.z};
}

Vec3 vec3_sub(Vec3 a, Vec3 b)
{
    return vec3_add(a, vec3_negate(b));
}

Vec3 vec3_mult(Vec3 a, Vec3 b)
{
    return (Vec3) {a.x * b.x, a.y * b.y, a.z * b.z};
}

Vec3 vec3_multf(Vec3 a, float b)
{
    return (Vec3) {a.x * b, a.y * b, a.z * b};
}

Vec3 vec3_div(Vec3 a, Vec3 b)
{
    return (Vec3) {a.x / b.x, a.y / b.y, a.z / b.z};
}

Vec3 vec3_divf(Vec3 a, float b)
{
    return vec3_multf(a, 1.f / b);
}

float vec3_dot(Vec3 a, Vec3 b)
{
    return (a.x * b.x + a.y * b.y + a.z * b.z);
}

Vec3 vec3_cross(Vec3 a, Vec3 b)
{
    return (Vec3) {a.y * b.z - a.z * b.y, a.z * b.x - a.x * b.z, a.x * b.y - a.y * b.x};
}

bool vec3_equals(Vec3 a, Vec3 b)
{
    return (a.x == b.x && a.y == b.y && a.z == b.z);
}

float vec3_lengthsq(Vec3 v)
{
    return (v.x * v.x + v.y * v.y + v.z * v.z);
}

float vec3_length(Vec3 v)
{
    return sqrtf(vec3_lengthsq(v));
}

Vec3 vec3_normalize(Vec3 v)
{
    Vec3 result = {0};

    float length = vec3_length(v);

    if (length != 0.f)
    {
        result = (Vec3) {
            .x = v.x * (1.f / length),
            .y = v.y * (1.f / length),
            .z = v.z * (1.f / length),
        };
    }

    return result;
}

Vec4 vec4(float x, float y, float z, float w)
{
    return (Vec4) {x, y, z, w};
}

Vec3 vec4_xyz(Vec4 v)
{
    return (Vec3) {v.x, v.y, v.z};
}

Vec4 vec4_multf(Vec4 a, float b)
{
    return (Vec4) {a.x * b, a.y * b, a.z * b, a.w * b};
}

Color color_red()
{
    return (Color) {1, 0, 0, 1};
}

Color color_blue()
{
    return (Color) {0, 1, 0, 1};
}

Color color_green()
{
    return (Color) {0, 0, 1, 1};
}

Color color_white()
{
    return (Color) {1, 1, 1, 1};
}

Color color_black()
{
    return (Color) {0, 0, 0, 1};
}

IntVec2 intvec2(int x, int y)
{
    return (IntVec2) {.x = x, .y = y};
}

IntVec2 intvec2_sub(IntVec2 a, IntVec2 b)
{
    IntVec2 result = intvec2(a.x - b.x, a.y - b.y);
    return result;
}

Mat4 mat4(float e00, float e01, float e02, float e03, float e10, float e11, float e12, float e13, float e20, float e21,
          float e22, float e23, float e30, float e31, float e32, float e33)
{
    return (Mat4)
    {
        {
            {e00, e10, e20, e30},
            {e01, e11, e21, e31},
            {e02, e12, e22, e32},
            {e03, e13, e23, e33}
        }
    };
}

Mat4 mat4_diagonal(float v)
{
    return mat4
    (
        v, 0, 0, 0,
        0, v, 0, 0,
        0, 0, v, 0,
        0, 0, 0, v
    );
}

Mat4 mat4_identity()
{
    return mat4_diagonal(1.f);
}

Mat4 mat4_transpose(Mat4 m)
{
    for (int column = 0; column < 4; ++column)
    {
        for (int row = 0; row < 4; ++row)
        {
            float tmp = m.e[column][row];
            m.e[column][row] = m.e[row][column];
            m.e[row][column] = tmp;
        }
    }
    return m;
}

Mat4 mat4_negate(Mat4 m)
{
    Mat4 result = {0};
    for (int column = 0; column < 4; ++column)
    {
        for (int row = 0; row < 4; ++row)
        {
            result.e[column][row] = -m.e[column][row];
        }
    }
    return result;
}

Mat4 mat4_add(Mat4 a, Mat4 b)
{
    Mat4 result = {0};
    for (int column = 0; column < 4; ++column)
    {
        for (int row = 0; row < 4; ++row)
        {
            result.e[column][row] = a.e[column][row] + b.e[column][row];
        }
    }
    return result;
}

Mat4 mat4_sub(Mat4 a, Mat4 b)
{
    return mat4_add(a, mat4_negate(b));
}

Mat4 mat4_mult(Mat4 a, Mat4 b)
{
    Mat4 result = {0};
    for (int column = 0; column < 4; ++column)
    {
        for (int row = 0; row < 4; ++row)
        {
            float sum = 0;
            for (int i = 0; i < 4; ++i)
            {
                sum += a.e[i][row] * b.e[column][i];
            }
            result.e[column][row] = sum;
        }
    }
    return result;
}

Mat4 mat4_multf(Mat4 a, float b)
{
    Mat4 result = {0};
    for (int column = 0; column < 4; ++column)
    {
        for (int row = 0; row < 4; ++row)
        {
            result.e[column][row] = a.e[column][row] * b;
        }
    }
    return result;
}

Vec4 mat4_mult_vec4(Mat4 a, Vec4 b)
{
    Vec4 result = {
        a.e[0][0] * b.x + a.e[1][0] * b.y + a.e[2][0] * b.z + a.e[3][0] * b.w,
        a.e[0][1] * b.x + a.e[1][1] * b.y + a.e[2][1] * b.z + a.e[3][1] * b.w,
        a.e[0][2] * b.x + a.e[1][2] * b.y + a.e[2][2] * b.z + a.e[3][2] * b.w,
        a.e[0][3] * b.x + a.e[1][3] * b.y + a.e[2][3] * b.z + a.e[3][3] * b.w
    };
    return result;
}

Mat4 mat4_ortho(float left, float right, float bottom, float top, float nearZ, float farZ)
{
    Mat4 result = {0};
    result.e[0][0] = 2.f / (right - left);
    result.e[1][1] = 2.f / (top - bottom);
    result.e[2][2] = 2.f / (nearZ - farZ);
    result.e[3][3] = 1.f;
    result.e[3][0] = (left + right) / (left - right);
    result.e[3][1] = (bottom + top) / (bottom - top);
    result.e[3][2] = (nearZ + farZ) / (nearZ - farZ);
    return result;
}

Mat4 mat4_persp(float fov, float aspect_ratio, float near_z, float far_z)
{
    assert_(near_z > 0.f);
    Mat4 result = { 0 };
    float tan_fov_over2 = tanf(to_radians(fov * 0.5f));
    result.e[0][0] = (1.f / tan_fov_over2) / aspect_ratio;
    result.e[1][1] = 1.f / tan_fov_over2;
    result.e[2][3] = -1.f;
    result.e[2][2] = -(near_z + far_z) / (far_z - near_z);
    result.e[3][2] = -(2.f * near_z * far_z) / (far_z - near_z);
    return result;
}

Mat4 mat4_translate(float x, float y, float z)
{
    Mat4 result = mat4_identity();
    result.e[3][0] = x;
    result.e[3][1] = y;
    result.e[3][2] = z;
    return result;
}

Mat4 mat4_translate_vec3(Vec3 t)
{
    Mat4 result = mat4_identity();
    result.e[3][0] = t.x;
    result.e[3][1] = t.y;
    result.e[3][2] = t.z;
    return result;
}

Mat4 mat4_scale(float x, float y, float z)
{
    Mat4 result = {0};
    result.e[0][0] = x;
    result.e[1][1] = y;
    result.e[2][2] = z;
    result.e[3][3] = 1.f;
    return result;
}

Mat4 mat4_scale_vec3(Vec3 s)
{
    Mat4 result = {0};
    result.e[0][0] = s.x;
    result.e[1][1] = s.y;
    result.e[2][2] = s.z;
    result.e[3][3] = 1.f;
    return result;
}

bool mat4_inverse(Mat4 in, Mat4* out)
{
    float inv[16];
    float det;
    int i;
    float* m = (float*)in.e;

    inv[0] = m[5] * m[10] * m[15] -
        m[5] * m[11] * m[14] -
        m[9] * m[6] * m[15] +
        m[9] * m[7] * m[14] +
        m[13] * m[6] * m[11] -
        m[13] * m[7] * m[10];

    inv[4] = -m[4] * m[10] * m[15] +
        m[4] * m[11] * m[14] +
        m[8] * m[6] * m[15] -
        m[8] * m[7] * m[14] -
        m[12] * m[6] * m[11] +
        m[12] * m[7] * m[10];

    inv[8] = m[4] * m[9] * m[15] -
        m[4] * m[11] * m[13] -
        m[8] * m[5] * m[15] +
        m[8] * m[7] * m[13] +
        m[12] * m[5] * m[11] -
        m[12] * m[7] * m[9];

    inv[12] = -m[4] * m[9] * m[14] +
        m[4] * m[10] * m[13] +
        m[8] * m[5] * m[14] -
        m[8] * m[6] * m[13] -
        m[12] * m[5] * m[10] +
        m[12] * m[6] * m[9];

    inv[1] = -m[1] * m[10] * m[15] +
        m[1] * m[11] * m[14] +
        m[9] * m[2] * m[15] -
        m[9] * m[3] * m[14] -
        m[13] * m[2] * m[11] +
        m[13] * m[3] * m[10];

    inv[5] = m[0] * m[10] * m[15] -
        m[0] * m[11] * m[14] -
        m[8] * m[2] * m[15] +
        m[8] * m[3] * m[14] +
        m[12] * m[2] * m[11] -
        m[12] * m[3] * m[10];

    inv[9] = -m[0] * m[9] * m[15] +
        m[0] * m[11] * m[13] +
        m[8] * m[1] * m[15] -
        m[8] * m[3] * m[13] -
        m[12] * m[1] * m[11] +
        m[12] * m[3] * m[9];

    inv[13] = m[0] * m[9] * m[14] -
        m[0] * m[10] * m[13] -
        m[8] * m[1] * m[14] +
        m[8] * m[2] * m[13] +
        m[12] * m[1] * m[10] -
        m[12] * m[2] * m[9];

    inv[2] = m[1] * m[6] * m[15] -
        m[1] * m[7] * m[14] -
        m[5] * m[2] * m[15] +
        m[5] * m[3] * m[14] +
        m[13] * m[2] * m[7] -
        m[13] * m[3] * m[6];

    inv[6] = -m[0] * m[6] * m[15] +
        m[0] * m[7] * m[14] +
        m[4] * m[2] * m[15] -
        m[4] * m[3] * m[14] -
        m[12] * m[2] * m[7] +
        m[12] * m[3] * m[6];

    inv[10] = m[0] * m[5] * m[15] -
        m[0] * m[7] * m[13] -
        m[4] * m[1] * m[15] +
        m[4] * m[3] * m[13] +
        m[12] * m[1] * m[7] -
        m[12] * m[3] * m[5];

    inv[14] = -m[0] * m[5] * m[14] +
        m[0] * m[6] * m[13] +
        m[4] * m[1] * m[14] -
        m[4] * m[2] * m[13] -
        m[12] * m[1] * m[6] +
        m[12] * m[2] * m[5];

    inv[3] = -m[1] * m[6] * m[11] +
        m[1] * m[7] * m[10] +
        m[5] * m[2] * m[11] -
        m[5] * m[3] * m[10] -
        m[9] * m[2] * m[7] +
        m[9] * m[3] * m[6];

    inv[7] = m[0] * m[6] * m[11] -
        m[0] * m[7] * m[10] -
        m[4] * m[2] * m[11] +
        m[4] * m[3] * m[10] +
        m[8] * m[2] * m[7] -
        m[8] * m[3] * m[6];

    inv[11] = -m[0] * m[5] * m[11] +
        m[0] * m[7] * m[9] +
        m[4] * m[1] * m[11] -
        m[4] * m[3] * m[9] -
        m[8] * m[1] * m[7] +
        m[8] * m[3] * m[5];

    inv[15] = m[0] * m[5] * m[10] -
        m[0] * m[6] * m[9] -
        m[4] * m[1] * m[10] +
        m[4] * m[2] * m[9] +
        m[8] * m[1] * m[6] -
        m[8] * m[2] * m[5];

    det = m[0] * inv[0] + m[1] * inv[4] + m[2] * inv[8] + m[3] * inv[12];

    if (det == 0)
        return false;

    det = 1.0f / det;

    float* invOut = (float*)out->e;
    for (i = 0; i < 16; i++)
        invOut[i] = inv[i] * det;

    return true;
}

Mat4 mat4_rotate(float angle, Vec3 axis)
{
    Mat4 result = mat4_identity();

    axis = vec3_normalize(axis);

    float radians = to_radians(angle);

    float sin_theta = sinf(to_radians(angle));
    float cos_theta = cosf(to_radians(angle));
    float cos_value = 1.0f - cos_theta;

    result.e[0][0] = (axis.x * axis.x * cos_value) + cos_theta;
    result.e[0][1] = (axis.x * axis.y * cos_value) + (axis.z * sin_theta);
    result.e[0][2] = (axis.x * axis.z * cos_value) - (axis.y * sin_theta);

    result.e[1][0] = (axis.y * axis.x * cos_value) - (axis.z * sin_theta);
    result.e[1][1] = (axis.y * axis.y * cos_value) + cos_theta;
    result.e[1][2] = (axis.y * axis.z * cos_value) + (axis.x * sin_theta);

    result.e[2][0] = (axis.z * axis.x * cos_value) + (axis.y * sin_theta);
    result.e[2][1] = (axis.z * axis.y * cos_value) - (axis.x * sin_theta);
    result.e[2][2] = (axis.z * axis.z * cos_value) + cos_theta;

    return result;
}

Mat4 mat4_lookat(Vec3 eye, Vec3 target, Vec3 up)
{
    Mat4 result = {0};
    Vec3 forward = vec3_normalize(vec3_sub(target, eye));
    Vec3 side = vec3_normalize(vec3_cross(forward, up));
    Vec3 top = vec3_cross(side, forward);

    result.e[0][0] = side.x;
    result.e[0][1] = top.x;
    result.e[0][2] = -forward.x;
    result.e[0][3] = 0.f;

    result.e[1][0] = side.y;
    result.e[1][1] = top.y;
    result.e[1][2] = -forward.y;
    result.e[1][3] = 0.f;

    result.e[2][0] = side.z;
    result.e[2][1] = top.z;
    result.e[2][2] = -forward.z;
    result.e[2][3] = 0.f;

    result.e[3][0] = -vec3_dot(side, eye);
    result.e[3][1] = -vec3_dot(top, eye);
    result.e[3][2] = vec3_dot(forward, eye);
    result.e[3][3] = 1.f;

    return result;
}

Quaternion quaternion(float x, float y, float z, float w)
{
    return (Quaternion){.x = x, .y = y, .z = z, .w = w};
}

Quaternion quaternion_vec4(Vec4 v)
{
    return quaternion(v.x, v.y, v.z, v.w);
}

Quaternion quaternion_add(Quaternion a, Quaternion b)
{
    Quaternion result = {
        .x = a.x + b.x,
        .y = a.y + b.y,
        .z = a.z + b.z,
        .w = a.w + b.w,
    };

    return result;
}

Quaternion quaternion_sub(Quaternion a, Quaternion b)
{
    Quaternion result = {
        .x = a.x - b.x,
        .y = a.y - b.y,
        .z = a.z - b.z,
        .w = a.w - b.w,
    };

    return result;
}

Vec3 screen_to_world(IntVec2 screen_pos, IntVec2 screen_size, Mat4 proj, Mat4 view)
{
    // TODO(illkwon): Handle to adjust by near plane z position (only for perspective projection)
    Mat4 vp = mat4_mult(proj, view);
    Mat4 inv_vp;
    bool inverse_result = mat4_inverse(vp, &inv_vp);
    assert_(inverse_result);

    Vec4 mouse_ndc_pos = vec4(
        2.f * (float)screen_pos.x / (float)screen_size.x - 1.f,
        2.f * (float)screen_pos.y / (float)screen_size.y - 1.f,
        -1.f,
        1.f);

    Vec4 world_pos_with_w = mat4_mult_vec4(inv_vp, mouse_ndc_pos);
    Vec3 world_pos = vec4_xyz(vec4_multf(world_pos_with_w, 1.f / world_pos_with_w.w));

    return world_pos;
}

Vec3 screen_to_world_ray(IntVec2 screen_pos, IntVec2 screen_size, Vec3 camera_pos, Mat4 proj, Mat4 view)
{
    Vec3 to = screen_to_world(screen_pos, screen_size, proj, view);
    return vec3_normalize(vec3_sub(to, camera_pos));
}
