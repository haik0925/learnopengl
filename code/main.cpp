#include "win32.h"
#include "common.h"
#include "debug.h"
#include "app.h"
#include "glad.h"
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"
#include <cmath>
#include <string>
#include <timeapi.h>
using namespace std;

#define target_chapter_index 6

#define init_main() \
    debug_set_global_log_buffer(malloc(megabytes(1)), megabytes(1)); \
    Win32Context win32 = win32_init(1280, 720); \
    win32_init_wgl(&win32); \
    timeBeginPeriod(1); \
    const float target_dt = 1.f / 60.f; \
    u64 last_counter = win32_current_clock(); \
    IntVec2 last_mouse_pos = {}; \
    win32_cursor_pos(&win32, &last_mouse_pos.x, &last_mouse_pos.y); \
    bool running = true;

#define run_main(process_key_func, update_func) \
    while (running) \
    { \
        AppEvent e = {}; \
        while (win32_poll_event(&e)) \
        { \
            if (e.type == AppEventType_Quit) \
            { \
                running = false; \
                break; \
            } \
            if (e.type == AppEventType_Keyupdown && !e.key.is_down && e.key.code == AppKeyCode_ESC) \
                running = false; \
            process_key_func(&e); \
        } \
        WindowSize window_size = win32_window_size(&win32); \
        IntVec2 mouse_pos; \
        win32_cursor_pos(&win32, &mouse_pos.x, &mouse_pos.y); \
        IntVec2 mouse_delta = intvec2_sub(mouse_pos, last_mouse_pos); \
        if (win32_is_window_focused()) \
        { \
            mouse_pos = intvec2(window_size.width / 2, window_size.height / 2); \
            win32_set_cursor_pos(&win32, mouse_pos.x, mouse_pos.y); \
        } \
        last_mouse_pos = mouse_pos; \
        win32_show_cursor(false); \
        glViewport(0, 0, window_size.width, window_size.height); \
        glClearColor(0.2f, 0.3f, 0.3f, 1.0f); \
        update(target_dt, mouse_delta); \
        Win32Clock counter = win32_current_clock(); \
        float dt = win32_elapsed_time(last_counter, counter); \
        int ms_to_sleep = (int)(target_dt * 1000.f) - (int)(dt * 1000.f + 2.f); \
        if (ms_to_sleep > 0 && dt < target_dt) \
            Sleep(ms_to_sleep); \
        counter = win32_current_clock(); \
        dt = win32_elapsed_time(last_counter, counter); \
        while (dt < target_dt) \
        { \
            counter = win32_current_clock(); \
            dt = win32_elapsed_time(last_counter, counter); \
        } \
        last_counter = counter; \
        SwapBuffers((HDC)win32.dc); \
    }

class Shader
{
public:
    GLuint _id;

    Shader(const GLchar* vs_source, const GLchar* fs_source)
    {
        GLuint vs = glCreateShader(GL_VERTEX_SHADER);
        {
            glShaderSource(vs, 1, &vs_source, nullptr);
            glCompileShader(vs);
            GLint success; glGetShaderiv(vs, GL_COMPILE_STATUS, &success);
            if (!success)
            {
                GLchar info_log[512];
                glGetShaderInfoLog(vs, sizeof(info_log), nullptr, info_log);
            }
        }

        GLuint fs = glCreateShader(GL_FRAGMENT_SHADER);
        {
            glShaderSource(fs ,1, &fs_source, nullptr);
            glCompileShader(fs);
            GLint success; glGetShaderiv(fs, GL_COMPILE_STATUS, &success);
            if (!success)
            {
                GLchar info_log[512];
                glGetShaderInfoLog(fs, sizeof(info_log), nullptr, info_log);
            }
        }

        GLuint shader_program = glCreateProgram();
        {
            glAttachShader(shader_program, vs);
            glAttachShader(shader_program, fs);
            glLinkProgram(shader_program);
            GLint success; glGetProgramiv(shader_program, GL_LINK_STATUS, &success);
            if (!success)
            {
                GLchar info_log[512];
                glGetProgramInfoLog(shader_program, sizeof(info_log), nullptr, info_log);
            }
        }

        _id = shader_program;

        glDeleteShader(vs);
        glDeleteShader(fs);
    }

    void use()
    {
        glUseProgram(_id);
    }

    void set_bool(string_view name, bool value) const
    {
        set_int(name, (int)value);
    }

    void set_int(string_view name, int value) const
    {
        GLint location = glGetUniformLocation(_id, name.data());
        glUniform1i(location, (int)value);
    }

    void set_float(string_view name, float value) const
    {
        GLint location = glGetUniformLocation(_id, name.data());
        glUniform1f(location, value);
    }

    void set_vec3(string_view name, Vec3 value) const
    {
        GLint location = glGetUniformLocation(_id, name.data());
        glUniform3f(location, value.x, value.y, value.z);
    }

    void set_mat4(string_view name, Mat4 value) const
    {
        GLint location = glGetUniformLocation(_id, name.data());
        glUniformMatrix4fv(location, 1, GL_FALSE, (float*)value.e);
    }
};

struct Camera
{
    Vec3 pos = {0, 0, 3};
    Vec3 up = {0, 1, 0};
    float yaw = -90.f;
    float pitch = 0.f;
    float move_speed = 5.f;

    void rotate_dir(float yaw_offset, float pitch_offset)
    {
        yaw += yaw_offset;
        pitch += pitch_offset;
        if (pitch > 89.f)
            pitch = 89.f;
        if (pitch < -89.f)
            pitch = -89.f;
    }

    Vec3 get_front() const
    {
        Vec3 result = vec3_normalize({
            cos(to_radians(pitch)) * cos(to_radians(yaw)),
            sin(to_radians(pitch)),
            cos(to_radians(pitch)) * sin(to_radians(yaw)),
        });
        return result;
    }

    Vec3 get_right() const
    {
        Vec3 result = vec3_normalize(vec3_cross(get_front(), up));
        return result;
    }

    Mat4 get_view() const
    {
        Mat4 result = mat4_lookat(pos, vec3_add(pos, get_front()), up);
        return result;
    }

    float get_fov(bool zoom) const
    {
        float result = zoom ? 10.f : 45.f;
        return result;
    }

    void update_move_xz(bool left, bool right, bool front, bool back, float dt)
    {
        if (left != right)
        {
            Vec3 movement = vec3_multf(
                vec3_normalize({get_right().x, 0, get_right().z}),
                move_speed * dt);
            if (left)
                pos = vec3_sub(pos, movement);
            if (right)
                pos = vec3_add(pos, movement);
        }
        if (front != back)
        {
            Vec3 movement = vec3_multf(
                vec3_normalize({get_front().x, 0, get_front().z}),
                move_speed * dt);
            if (front)
                pos = vec3_add(pos, movement);
            if (back)
                pos = vec3_sub(pos, movement);
        }
    }

    void update_move(bool left, bool right, bool front, bool back, float dt)
    {
        if (left != right)
        {
            Vec3 movement = vec3_multf(get_right(), move_speed * dt);
            if (left)
                pos = vec3_sub(pos, movement);
            if (right)
                pos = vec3_add(pos, movement);
        }
        if (front != back)
        {
            Vec3 movement = vec3_multf(get_front(), move_speed * dt);
            if (front)
                pos = vec3_add(pos, movement);
            if (back)
                pos = vec3_sub(pos, movement);
        }
    }
};

#if target_chapter_index == 0
int main()
{
    init_main();

    const char* vs_source =
R"(#version 330 core
layout (location = 0) in vec3 aPos;
void main()
{
    gl_Position = vec4(aPos.x, aPos.y, aPos.z, 1.0);
}
)";

    const char* fs_source =
R"(#version 330 core
out vec4 FragColor;
void main()
{
    FragColor = vec4(1.0f, 0.5f, 0.2f, 1.0f);
}
)";

    const char* fs_source2 =
R"(#version 330 core
out vec4 FragColor;
void main()
{
    FragColor = vec4(1.0f, 1.0f, 0.0f, 1.0f);
}
)";

    GLuint vs = glCreateShader(GL_VERTEX_SHADER);
    {
        glShaderSource(vs, 1, &vs_source, nullptr);
        glCompileShader(vs);
        GLint success; glGetShaderiv(vs, GL_COMPILE_STATUS, &success);
        if (!success)
        {
            GLchar info_log[512];
            glGetShaderInfoLog(vs, sizeof(info_log), nullptr, info_log);
        }
    }

    GLuint fs = glCreateShader(GL_FRAGMENT_SHADER);
    {
        glShaderSource(fs ,1, &fs_source, nullptr);
        glCompileShader(fs);
        GLint success; glGetShaderiv(fs, GL_COMPILE_STATUS, &success);
        if (!success)
        {
            GLchar info_log[512];
            glGetShaderInfoLog(fs, sizeof(info_log), nullptr, info_log);
        }
    }

    GLuint shader_program = glCreateProgram();
    {
        glAttachShader(shader_program, vs);
        glAttachShader(shader_program, fs);
        glLinkProgram(shader_program);
        GLint success; glGetProgramiv(shader_program, GL_LINK_STATUS, &success);
        if (!success)
        {
            GLchar info_log[512];
            glGetProgramInfoLog(shader_program, sizeof(info_log), nullptr, info_log);
        }
    }

    GLuint fs2 = glCreateShader(GL_FRAGMENT_SHADER);
    {
        glShaderSource(fs2 ,1, &fs_source2, nullptr);
        glCompileShader(fs2);
        GLint success; glGetShaderiv(fs2, GL_COMPILE_STATUS, &success);
        if (!success)
        {
            GLchar info_log[512];
            glGetShaderInfoLog(fs2, sizeof(info_log), nullptr, info_log);
        }
    }

    GLuint shader_program2 = glCreateProgram();
    {
        glAttachShader(shader_program2, vs);
        glAttachShader(shader_program2, fs2);
        glLinkProgram(shader_program2);
        GLint success; glGetProgramiv(shader_program2, GL_LINK_STATUS, &success);
        if (!success)
        {
            GLchar info_log[512];
            glGetProgramInfoLog(shader_program2, sizeof(info_log), nullptr, info_log);
        }
    }

    float vertices[] = {
        0.5f, 0.5f, 0.0f, // top right
        0.5f, -0.5f, 0.0f, // bottom right
        -0.5f, -0.5f, 0.0f, // bottom left
        -0.5f, 0.5f, 0.0f, // top left
    };

    uint indices[] = {
        0, 1, 3,
        1, 2, 3,
    };

    GLuint ebo;
    glGenBuffers(1, &ebo);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

    GLuint vbo;
    glGenBuffers(1, &vbo);

    GLuint vao;
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);

    float two_tris_vertices[] = {
        -1.0f, 0.0f, 0.0f,
        -0.625f, 0.75f, 0.0f,
        -0.25f, 0.0f, 0.0f,

        -0.75f, 0.0f, 0.0f,
        -0.375f, -0.75f, 0.0f,
        0.0, 0.0f, 0.0f,
    };
    GLuint two_tris_vbo;
    glGenBuffers(1, &two_tris_vbo);
    GLuint two_tris_vao;
    glGenVertexArrays(1, &two_tris_vao);
    glBindVertexArray(two_tris_vao);
    glBindBuffer(GL_ARRAY_BUFFER, two_tris_vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(two_tris_vertices), two_tris_vertices, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);

    float first_tri[] = {
        1.0f, 0.0f, 0.0f,
        0.625f, 0.75f, 0.0f,
        0.25f, 0.0f, 0.0f,
    };

    float second_tri[] = {
        0.75f, 0.0f, 0.0f,
        0.375f, -0.75f, 0.0f,
        0.0, 0.0f, 0.0f,
    };
    GLuint first_tri_vao;
    glGenVertexArrays(1, &first_tri_vao);
    glBindVertexArray(first_tri_vao);
    GLuint first_tri_vbo;
    glGenBuffers(1, &first_tri_vbo);
    glBindBuffer(GL_ARRAY_BUFFER, first_tri_vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(first_tri), first_tri, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);

    GLuint second_tri_vao;
    glGenVertexArrays(1, &second_tri_vao);
    glBindVertexArray(second_tri_vao);
    GLuint second_tri_vbo;
    glGenBuffers(1, &second_tri_vbo);
    glBindBuffer(GL_ARRAY_BUFFER, second_tri_vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(second_tri), second_tri, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);

    GLuint current_shader_program = shader_program;
    GLenum draw_mode = GL_FILL;

    auto process_key = [&](AppEvent* e)
    {
        switch (e->type)
        {
        case AppEventType_Quit:
            running = false;
            break;
        case AppEventType_Keyupdown:
            if (!e->key.is_down)
            {
                switch (e->key.code)
                {
                case AppKeyCode_1:
                    draw_mode = draw_mode == GL_FILL ? GL_LINE : GL_FILL;
                    break;
                case AppKeyCode_2:
                    current_shader_program =
                        current_shader_program == shader_program ?
                        shader_program2 : shader_program;
                    break;
                }
            }
        }
    };

    auto update = [&](float dt, IntVec2 mouse_delta)
    {
        glClear(GL_COLOR_BUFFER_BIT);
        glPolygonMode(GL_FRONT_AND_BACK, draw_mode);
        glUseProgram(current_shader_program);
        glBindVertexArray(vao);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, nullptr);
        glBindVertexArray(two_tris_vao);
        glDrawArrays(GL_TRIANGLES, 0, 6);
        glBindVertexArray(first_tri_vao);
        glDrawArrays(GL_TRIANGLES, 0, 3);
        glBindVertexArray(second_tri_vao);
        glDrawArrays(GL_TRIANGLES, 0, 3);
    };

    run_main(process_key, update);

    return 0;
}
#elif target_chapter_index == 1
int main()
{
    init_main();

    GLint max_vert_attrib_count;
    glGetIntegerv(GL_MAX_VERTEX_ATTRIBS, &max_vert_attrib_count);
    log_("Max vertex attribute count supported: %d", max_vert_attrib_count);

    const char* vs_source =
R"(#version 330 core
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aColor;

out vec3 ourColor;

uniform float horizonOffset;

void main()
{
    gl_Position = vec4(aPos.x + horizonOffset, -aPos.y, aPos.z, 1.0);
    ourColor = gl_Position.xyz;
}
)";

    const char* fs_source =
R"(#version 330 core
out vec4 FragColor;
in vec3 ourColor;

void main()
{
    FragColor = vec4(ourColor, 1.0);
}
)";

    Shader shader(vs_source, fs_source);

    float vertices[] = {
        -0.5f, -0.5f, 0.0f, 1.0f, 0.0f, 0.0f,
        0.5f, -0.5f, 0.0f, 0.0f, 1.0f, 0.0f,
        0.0f, 0.5f, 0.0f, 0.0f, 0.0f, 1.0f,
    };

    GLuint vao;
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);
    GLuint vbo;
    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)(3 * sizeof(float)));
    glEnableVertexAttribArray(1);

    auto pk = [&](AppEvent* e)
    {
        switch (e->type)
        {
        case AppEventType_Quit:
            running = false;
            break;
        case AppEventType_Keyupdown:
            if (!e->key.is_down)
            {
                switch (e->key.code)
                {
                case AppKeyCode_1:
                    break;
                case AppKeyCode_2:
                    break;
                }
            }
        }
    };

    float time = 0.f;

    auto update = [&](float dt, IntVec2 mouse_delta)
    {
        glClear(GL_COLOR_BUFFER_BIT);
        time += 0.0005f;
        if (time >= 2 * math_pi32)
            time -= 2 * math_pi32;

        GLfloat horizon_offset = sin(time) / 2.f;

        shader.use();
        shader.set_float("horizonOffset", horizon_offset);
        glBindVertexArray(vao);
        glDrawArrays(GL_TRIANGLES, 0, 3);
    };
    run_main(pk, update);
}

// Chapter: Textures
#elif target_chapter_index == 2

int main()
{
    init_main();

    stbi_set_flip_vertically_on_load(true);

    GLuint wooden_container_texture;
    glGenTextures(1, &wooden_container_texture);
    glBindTexture(GL_TEXTURE_2D, wooden_container_texture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    int wooden_container_width, wooden_container_height, wooden_container_num_channels;
    u8* wooden_container_image = stbi_load("container.jpg", &wooden_container_width, &wooden_container_height, &wooden_container_num_channels, 0);
    if (wooden_container_image)
    {
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, wooden_container_width, wooden_container_height,
            0, GL_RGB, GL_UNSIGNED_BYTE, wooden_container_image);
    }

    GLuint awesomeface_texture;
    glGenTextures(1, &awesomeface_texture);
    glBindTexture(GL_TEXTURE_2D, awesomeface_texture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    float border_color[] = {1.f, 1.f, 0.f, 1.f};
    glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, border_color);

    int awesomeface_width, awesomeface_height, awesomeface_num_channels;
    u8* awesomeface_image = stbi_load("awesomeface.png", &awesomeface_width, &awesomeface_height, &awesomeface_num_channels, 0);
    if (awesomeface_image)
    {
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, awesomeface_width, awesomeface_height,
            0, GL_RGBA, GL_UNSIGNED_BYTE, awesomeface_image);
    }

    float vertices[] = {
         0.5f,  0.5f, 0.0f, 1.0f, 0.0f, 0.0f, 2.0f, 2.0f,// top right
         0.5f, -0.5f, 0.0f, 0.0f, 1.0f, 0.0f, 2.0f, 0.0f,// bottom right
        -0.5f, -0.5f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f,// bottom left
        -0.5f,  0.5f, 0.0f, 1.0f, 1.0f, 0.0f, 0.0f, 2.0f,// top left
    };
    GLuint vao;
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);
    GLuint vbo;
    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(3 * sizeof(float)));
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(6 * sizeof(float)));
    glEnableVertexAttribArray(2);

    int indices[] = {
        3, 0, 1,
        1, 2, 3
    };
    GLuint ebo;
    glGenBuffers(1, &ebo);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);

    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

    const char *vs_src =
R"(#version 330 core
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aColor;
layout (location = 2) in vec2 aTexCoord;

out vec3 ourColor;
out vec2 TexCoord;

void main()
{
    gl_Position = vec4(aPos, 1.0);
    ourColor = aColor;
    TexCoord = aTexCoord;
}
)";

    Shader shader(vs_src,
R"(#version 330 core
out vec4 FragColor;
in vec3 ourColor;
in vec2 TexCoord;
uniform sampler2D texture1;
uniform sampler2D texture2;
void main()
{
    FragColor = mix(texture(texture1, TexCoord), texture(texture2, TexCoord), 0.2);
}
)");
    shader.use();
    shader.set_int("texture1", 0);
    shader.set_int("texture2", 1);

    Shader shader2(vs_src,
R"(#version 330 core
out vec4 FragColor;
in vec3 ourColor;
in vec2 TexCoord;
uniform sampler2D texture1;
uniform sampler2D texture2;
void main()
{
    FragColor = mix(texture(texture1, TexCoord), texture(texture2, vec2(TexCoord.x, 1.0 - TexCoord.y)), 0.2);
}
)");
    shader2.use();
    shader2.set_int("texture1", 0);
    shader2.set_int("texture2", 1);

    Shader* current_shader = &shader;

    auto pk = [&](AppEvent* e)
    {
        if (e->type == AppEventType_Keyupdown && !e->key.is_down && e->key.code == AppKeyCode_1)
            current_shader = current_shader == &shader ? &shader2 : &shader;
    };
    auto update = [&](float dt, IntVec2 mouse_delta)
    {
        glClear(GL_COLOR_BUFFER_BIT);
        current_shader->use();
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, wooden_container_texture);
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, awesomeface_texture);
        glBindVertexArray(vao);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, nullptr);
    };
    run_main(pk, update);

    stbi_image_free(awesomeface_image);
    stbi_image_free(wooden_container_image);
}

// Chapter: Coordinate Systems
#elif target_chapter_index == 3

int main()
{
    init_main();

    stbi_set_flip_vertically_on_load(true);

    GLuint wooden_container_texture;
    glGenTextures(1, &wooden_container_texture);
    glBindTexture(GL_TEXTURE_2D, wooden_container_texture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    int wooden_container_width, wooden_container_height, wooden_container_num_channels;
    u8* wooden_container_image = stbi_load("container.jpg", &wooden_container_width, &wooden_container_height, &wooden_container_num_channels, 0);
    if (wooden_container_image)
    {
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, wooden_container_width, wooden_container_height,
            0, GL_RGB, GL_UNSIGNED_BYTE, wooden_container_image);
    }

    GLuint awesomeface_texture;
    glGenTextures(1, &awesomeface_texture);
    glBindTexture(GL_TEXTURE_2D, awesomeface_texture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    float border_color[] = {1.f, 1.f, 0.f, 1.f};
    glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, border_color);

    int awesomeface_width, awesomeface_height, awesomeface_num_channels;
    u8* awesomeface_image = stbi_load("awesomeface.png", &awesomeface_width, &awesomeface_height, &awesomeface_num_channels, 0);
    if (awesomeface_image)
    {
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, awesomeface_width, awesomeface_height,
            0, GL_RGBA, GL_UNSIGNED_BYTE, awesomeface_image);
    }

    float vertices[] = {
        -0.5f, -0.5f, -0.5f,  0.0f, 0.0f,
         0.5f, -0.5f, -0.5f,  1.0f, 0.0f,
         0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
         0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
        -0.5f,  0.5f, -0.5f,  0.0f, 1.0f,
        -0.5f, -0.5f, -0.5f,  0.0f, 0.0f,

        -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
         0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
         0.5f,  0.5f,  0.5f,  1.0f, 1.0f,
         0.5f,  0.5f,  0.5f,  1.0f, 1.0f,
        -0.5f,  0.5f,  0.5f,  0.0f, 1.0f,
        -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,

        -0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
        -0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
        -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
        -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
        -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
        -0.5f,  0.5f,  0.5f,  1.0f, 0.0f,

         0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
         0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
         0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
         0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
         0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
         0.5f,  0.5f,  0.5f,  1.0f, 0.0f,

        -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
         0.5f, -0.5f, -0.5f,  1.0f, 1.0f,
         0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
         0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
        -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
        -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,

        -0.5f,  0.5f, -0.5f,  0.0f, 1.0f,
         0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
         0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
         0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
        -0.5f,  0.5f,  0.5f,  0.0f, 0.0f,
        -0.5f,  0.5f, -0.5f,  0.0f, 1.0f
    };

    GLuint vao;
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);
    GLuint vbo;
    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));
    glEnableVertexAttribArray(1);

    const char *vs_src =
R"(#version 330 core
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec2 aTexCoord;

out vec2 TexCoord;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

void main()
{
    gl_Position = projection * view * model * vec4(aPos, 1.0);
    TexCoord = aTexCoord;
}
)";

    Shader shader(vs_src,
R"(#version 330 core
out vec4 FragColor;
in vec2 TexCoord;
uniform sampler2D texture1;
uniform sampler2D texture2;
void main()
{
    FragColor = mix(texture(texture1, TexCoord), texture(texture2, TexCoord), 0.2);
}
)");
    shader.use();
    shader.set_int("texture1", 0);
    shader.set_int("texture2", 1);

    Shader shader2(vs_src,
R"(#version 330 core
out vec4 FragColor;
in vec2 TexCoord;
uniform sampler2D texture1;
uniform sampler2D texture2;
void main()
{
    FragColor = mix(texture(texture1, TexCoord), texture(texture2, vec2(TexCoord.x, 1.0 - TexCoord.y)), 0.2);
}
)");
    shader2.use();
    shader2.set_int("texture1", 0);
    shader2.set_int("texture2", 1);

    Shader* current_shader = &shader;

    Vec3 cube_positions[] = {
        { 0.0f,  0.0f,  0.0f},
        { 2.0f,  5.0f, -15.0f},
        {-1.5f, -2.2f, -2.5f},
        {-3.8f, -2.0f, -12.3f},
        { 2.4f, -0.4f, -3.5f},
        {-1.7f,  3.0f, -7.5f},
        { 1.3f, -2.0f, -2.5f},
        { 1.5f,  2.0f, -2.5f},
        { 1.5f,  0.2f, -1.5f},
        {-1.3f,  1.0f, -1.5f},
    };

    Mat4 view = mat4_translate(0, 0, -3);

    float timer = 0.f;

    glEnable(GL_DEPTH_TEST);

    auto pk = [&](AppEvent* e)
    {
        if (e->type == AppEventType_Keyupdown && !e->key.is_down && e->key.code == AppKeyCode_1)
            current_shader = current_shader == &shader ? &shader2 : &shader;
    };
    auto update = [&](float dt, IntVec2 mouse_delta)
    {
        timer += dt;

        WindowSize window_size = win32_window_size(&win32);
        Mat4 projection = mat4_persp(45.f, (float)window_size.x / (float)window_size.y, 0.1f, 100.f);

        glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
        current_shader->use();
        current_shader->set_mat4("view", view);
        current_shader->set_mat4("projection", projection);
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, wooden_container_texture);
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, awesomeface_texture);
        glBindVertexArray(vao);
        for (int i = 0; i < array_count(cube_positions); ++i)
        {
            Mat4 model = mat4_translate_vec3(cube_positions[i]);
            float angle = 20.f * (float)i;
            model = mat4_mult(model, mat4_rotate(i % 3 == 0 ? timer * angle : angle, {1.f, 0.3f, 0.5f}));
            current_shader->set_mat4("model", model);
            glDrawArrays(GL_TRIANGLES, 0, 36);
        }
    };
    run_main(pk, update);

    stbi_image_free(awesomeface_image);
    stbi_image_free(wooden_container_image);
}

// Chapter: Camera
#elif target_chapter_index == 4

int main()
{
    init_main();

    stbi_set_flip_vertically_on_load(true);

    GLuint wooden_container_texture;
    glGenTextures(1, &wooden_container_texture);
    glBindTexture(GL_TEXTURE_2D, wooden_container_texture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    int wooden_container_width, wooden_container_height, wooden_container_num_channels;
    u8* wooden_container_image = stbi_load("container.jpg", &wooden_container_width, &wooden_container_height, &wooden_container_num_channels, 0);
    if (wooden_container_image)
    {
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, wooden_container_width, wooden_container_height,
            0, GL_RGB, GL_UNSIGNED_BYTE, wooden_container_image);
    }

    GLuint awesomeface_texture;
    glGenTextures(1, &awesomeface_texture);
    glBindTexture(GL_TEXTURE_2D, awesomeface_texture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    float border_color[] = {1.f, 1.f, 0.f, 1.f};
    glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, border_color);

    int awesomeface_width, awesomeface_height, awesomeface_num_channels;
    u8* awesomeface_image = stbi_load("awesomeface.png", &awesomeface_width, &awesomeface_height, &awesomeface_num_channels, 0);
    if (awesomeface_image)
    {
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, awesomeface_width, awesomeface_height,
            0, GL_RGBA, GL_UNSIGNED_BYTE, awesomeface_image);
    }

    float vertices[] = {
        -0.5f, -0.5f, -0.5f,  0.0f, 0.0f,
         0.5f, -0.5f, -0.5f,  1.0f, 0.0f,
         0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
         0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
        -0.5f,  0.5f, -0.5f,  0.0f, 1.0f,
        -0.5f, -0.5f, -0.5f,  0.0f, 0.0f,

        -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
         0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
         0.5f,  0.5f,  0.5f,  1.0f, 1.0f,
         0.5f,  0.5f,  0.5f,  1.0f, 1.0f,
        -0.5f,  0.5f,  0.5f,  0.0f, 1.0f,
        -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,

        -0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
        -0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
        -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
        -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
        -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
        -0.5f,  0.5f,  0.5f,  1.0f, 0.0f,

         0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
         0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
         0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
         0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
         0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
         0.5f,  0.5f,  0.5f,  1.0f, 0.0f,

        -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
         0.5f, -0.5f, -0.5f,  1.0f, 1.0f,
         0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
         0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
        -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
        -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,

        -0.5f,  0.5f, -0.5f,  0.0f, 1.0f,
         0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
         0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
         0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
        -0.5f,  0.5f,  0.5f,  0.0f, 0.0f,
        -0.5f,  0.5f, -0.5f,  0.0f, 1.0f
    };

    GLuint vao;
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);
    GLuint vbo;
    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));
    glEnableVertexAttribArray(1);

    const char *vs_src =
R"(#version 330 core
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec2 aTexCoord;

out vec2 TexCoord;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

void main()
{
    gl_Position = projection * view * model * vec4(aPos, 1.0);
    TexCoord = aTexCoord;
}
)";

    Shader shader(vs_src,
R"(#version 330 core
out vec4 FragColor;
in vec2 TexCoord;
uniform sampler2D texture1;
uniform sampler2D texture2;
void main()
{
    FragColor = mix(texture(texture1, TexCoord), texture(texture2, TexCoord), 0.2);
}
)");
    shader.use();
    shader.set_int("texture1", 0);
    shader.set_int("texture2", 1);

    Shader shader2(vs_src,
R"(#version 330 core
out vec4 FragColor;
in vec2 TexCoord;
uniform sampler2D texture1;
uniform sampler2D texture2;
void main()
{
    FragColor = mix(texture(texture1, TexCoord), texture(texture2, vec2(TexCoord.x, 1.0 - TexCoord.y)), 0.2);
}
)");
    shader2.use();
    shader2.set_int("texture1", 0);
    shader2.set_int("texture2", 1);

    Shader* current_shader = &shader;

    Vec3 cube_positions[] = {
        { 0.0f,  0.0f,  0.0f},
        { 2.0f,  5.0f, -15.0f},
        {-1.5f, -2.2f, -2.5f},
        {-3.8f, -2.0f, -12.3f},
        { 2.4f, -0.4f, -3.5f},
        {-1.7f,  3.0f, -7.5f},
        { 1.3f, -2.0f, -2.5f},
        { 1.5f,  2.0f, -2.5f},
        { 1.5f,  0.2f, -1.5f},
        {-1.3f,  1.0f, -1.5f},
    };

    Camera camera = {};

    bool left = false;
    bool right = false;
    bool front = false;
    bool back = false;
    bool zoom = false;

    float timer = 0.f;

    glEnable(GL_DEPTH_TEST);

    auto pk = [&](AppEvent* e)
    {
        if (e->type == AppEventType_Keyupdown)
        {
            if (!e->key.is_down && e->key.code == AppKeyCode_1)
                current_shader = current_shader == &shader ? &shader2 : &shader;

            switch (e->key.code)
            {
            case AppKeyCode_A:
                left = e->key.is_down;
                break;
            case AppKeyCode_D:
                right = e->key.is_down;
                break;
            case AppKeyCode_W:
                front = e->key.is_down;
                break;
            case AppKeyCode_S:
                back = e->key.is_down;
                break;
            case AppKeyCode_LMouse:
                zoom = e->key.is_down;
                break;
            }
        }
    };

    auto update = [&](float dt, IntVec2 mouse_delta)
    {
        timer += dt;

        WindowSize window_size = win32_window_size(&win32);

        camera.rotate_dir(mouse_delta.x * dt, mouse_delta.y * dt);
        camera.update_move_xz(left, right, front, back, dt);

        Mat4 projection = mat4_persp(camera.get_fov(zoom), (float)window_size.x / (float)window_size.y, 0.1f, 100.f);

        glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
        current_shader->use();
        current_shader->set_mat4("view", camera.get_view());
        current_shader->set_mat4("projection", projection);
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, wooden_container_texture);
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, awesomeface_texture);
        glBindVertexArray(vao);
        for (int i = 0; i < array_count(cube_positions); ++i)
        {
            Mat4 model = mat4_translate_vec3(cube_positions[i]);
            float angle = 20.f * (float)i;
            model = mat4_mult(model, mat4_rotate(i % 3 == 0 ? timer * angle : angle, {1.f, 0.3f, 0.5f}));
            current_shader->set_mat4("model", model);
            glDrawArrays(GL_TRIANGLES, 0, 36);
        }
    };
    run_main(pk, update);

    stbi_image_free(awesomeface_image);
    stbi_image_free(wooden_container_image);
}

// Chapter: Colors
#elif target_chapter_index == 5

int main()
{
    init_main();

    float vertices[] = {
        -0.5f, -0.5f, -0.5f,
         0.5f, -0.5f, -0.5f,
         0.5f,  0.5f, -0.5f,
         0.5f,  0.5f, -0.5f,
        -0.5f,  0.5f, -0.5f,
        -0.5f, -0.5f, -0.5f,

        -0.5f, -0.5f,  0.5f,
         0.5f, -0.5f,  0.5f,
         0.5f,  0.5f,  0.5f,
         0.5f,  0.5f,  0.5f,
        -0.5f,  0.5f,  0.5f,
        -0.5f, -0.5f,  0.5f,

        -0.5f,  0.5f,  0.5f,
        -0.5f,  0.5f, -0.5f,
        -0.5f, -0.5f, -0.5f,
        -0.5f, -0.5f, -0.5f,
        -0.5f, -0.5f,  0.5f,
        -0.5f,  0.5f,  0.5f,

         0.5f,  0.5f,  0.5f,
         0.5f,  0.5f, -0.5f,
         0.5f, -0.5f, -0.5f,
         0.5f, -0.5f, -0.5f,
         0.5f, -0.5f,  0.5f,
         0.5f,  0.5f,  0.5f,

        -0.5f, -0.5f, -0.5f,
         0.5f, -0.5f, -0.5f,
         0.5f, -0.5f,  0.5f,
         0.5f, -0.5f,  0.5f,
        -0.5f, -0.5f,  0.5f,
        -0.5f, -0.5f, -0.5f,

        -0.5f,  0.5f, -0.5f,
         0.5f,  0.5f, -0.5f,
         0.5f,  0.5f,  0.5f,
         0.5f,  0.5f,  0.5f,
        -0.5f,  0.5f,  0.5f,
        -0.5f,  0.5f, -0.5f,
    };

    GLuint vbo;
    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    GLuint light_source_vao;
    glGenVertexArrays(1, &light_source_vao);
    glBindVertexArray(light_source_vao);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);

    GLuint light_target_vao;
    glGenVertexArrays(1, &light_target_vao);
    glBindVertexArray(light_target_vao);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);

    const char *vs_src =
R"(#version 330 core
layout (location = 0) in vec3 aPos;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

void main()
{
    gl_Position = projection * view * model * vec4(aPos, 1.0);
}
)";

    Shader light_target_shader(vs_src,
R"(#version 330 core
out vec4 FragColor;

uniform vec3 objectColor;
uniform vec3 lightColor;

void main()
{
    FragColor = vec4(objectColor * lightColor, 1.0);
}
)");
    light_target_shader.use();
    light_target_shader.set_vec3("objectColor", { 1.f, 0.5f, 0.31f });
    light_target_shader.set_vec3("lightColor", { 1.f, 1.f, 1.f });

    Shader light_source_shader(vs_src,
R"(#version 330 core
out vec4 FragColor;

void main()
{
    FragColor = vec4(1.0);
}
)");

    Camera camera = {};

    bool left = false;
    bool right = false;
    bool front = false;
    bool back = false;
    bool zoom = false;

    float timer = 0.f;

    glEnable(GL_DEPTH_TEST);

    auto pk = [&](AppEvent* e)
    {
        if (e->type == AppEventType_Keyupdown)
        {
            switch (e->key.code)
            {
            case AppKeyCode_A:
                left = e->key.is_down;
                break;
            case AppKeyCode_D:
                right = e->key.is_down;
                break;
            case AppKeyCode_W:
                front = e->key.is_down;
                break;
            case AppKeyCode_S:
                back = e->key.is_down;
                break;
            case AppKeyCode_LMouse:
                zoom = e->key.is_down;
                break;
            }
        }
    };

    auto update = [&](float dt, IntVec2 mouse_delta)
    {
        timer += dt;

        WindowSize window_size = win32_window_size(&win32);

        camera.rotate_dir(mouse_delta.x * dt, mouse_delta.y * dt);
        camera.update_move_xz(left, right, front, back, dt);

        Mat4 projection = mat4_persp(camera.get_fov(zoom), (float)window_size.x / (float)window_size.y, 0.1f, 100.f);

        glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

        light_target_shader.use();
        light_target_shader.set_mat4("model", mat4_identity());
        light_target_shader.set_mat4("view", camera.get_view());
        light_target_shader.set_mat4("projection", projection);
        glBindVertexArray(light_target_vao);
        glDrawArrays(GL_TRIANGLES, 0, 36);

        Mat4 model = mat4_translate_vec3({ 1.2f, 1.f, 2.f });
        model = mat4_mult(model, mat4_scale(0.2f, 0.2f, 0.2f));
        light_source_shader.use();
        light_source_shader.set_mat4("model", model);
        light_source_shader.set_mat4("view", camera.get_view());
        light_source_shader.set_mat4("projection", projection);
        glBindVertexArray(light_source_vao);
        glDrawArrays(GL_TRIANGLES, 0, 36);
    };
    run_main(pk, update);
}

// Chapter: Basic Lighting
#elif target_chapter_index == 6

int main()
{
    init_main();

    float vertices[] = {
        -0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,
         0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,
         0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,
         0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,
        -0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,
        -0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,

        -0.5f, -0.5f,  0.5f,  0.0f,  0.0f, 1.0f,
         0.5f, -0.5f,  0.5f,  0.0f,  0.0f, 1.0f,
         0.5f,  0.5f,  0.5f,  0.0f,  0.0f, 1.0f,
         0.5f,  0.5f,  0.5f,  0.0f,  0.0f, 1.0f,
        -0.5f,  0.5f,  0.5f,  0.0f,  0.0f, 1.0f,
        -0.5f, -0.5f,  0.5f,  0.0f,  0.0f, 1.0f,

        -0.5f,  0.5f,  0.5f, -1.0f,  0.0f,  0.0f,
        -0.5f,  0.5f, -0.5f, -1.0f,  0.0f,  0.0f,
        -0.5f, -0.5f, -0.5f, -1.0f,  0.0f,  0.0f,
        -0.5f, -0.5f, -0.5f, -1.0f,  0.0f,  0.0f,
        -0.5f, -0.5f,  0.5f, -1.0f,  0.0f,  0.0f,
        -0.5f,  0.5f,  0.5f, -1.0f,  0.0f,  0.0f,

         0.5f,  0.5f,  0.5f,  1.0f,  0.0f,  0.0f,
         0.5f,  0.5f, -0.5f,  1.0f,  0.0f,  0.0f,
         0.5f, -0.5f, -0.5f,  1.0f,  0.0f,  0.0f,
         0.5f, -0.5f, -0.5f,  1.0f,  0.0f,  0.0f,
         0.5f, -0.5f,  0.5f,  1.0f,  0.0f,  0.0f,
         0.5f,  0.5f,  0.5f,  1.0f,  0.0f,  0.0f,

        -0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,
         0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,
         0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,
         0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,
        -0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,
        -0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,

        -0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,
         0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,
         0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,
         0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,
        -0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,
        -0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f
    };

    GLuint vbo;
    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    GLuint light_source_vao;
    glGenVertexArrays(1, &light_source_vao);
    glBindVertexArray(light_source_vao);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);

    GLuint light_target_vao;
    glGenVertexArrays(1, &light_target_vao);
    glBindVertexArray(light_target_vao);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_TRUE, 6 * sizeof(float), (void*)(3 * sizeof(float)));
    glEnableVertexAttribArray(1);

    Shader light_target_shader_phong(
R"(#version 330 core
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aNormal;

out vec3 FragPos;
out vec3 Normal;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

void main()
{
    gl_Position = projection * view * model * vec4(aPos, 1.0);
    FragPos = vec3(view * model * vec4(aPos, 1.0));
    Normal = mat3(transpose(inverse(view * model))) * aNormal;
}
)",
R"(#version 330 core
in vec3 FragPos;
in vec3 Normal;

out vec4 FragColor;

uniform mat4 view;
uniform vec3 objectColor;
uniform vec3 lightColor;
uniform vec3 lightPos;
// uniform vec3 viewPos;

void main()
{
    vec3 viewPos = vec3(0.0);
    vec3 lp = vec3(view * vec4(lightPos, 1.0));

    float ambientStrength = 0.1;
    vec3 ambient = ambientStrength * lightColor;

    vec3 lightDir = normalize(lp - FragPos);
    vec3 norm = normalize(Normal);
    float diff = max(dot(lightDir, norm), 0.0);
    vec3 diffuse = diff * lightColor;

    float specularStrength = 0.5;
    vec3 viewDir = normalize(viewPos - FragPos);
    vec3 reflectDir = reflect(-lightDir, norm);

    float spec = pow(max(dot(viewDir, reflectDir), 0.0), 32);
    vec3 specular = specularStrength * spec * lightColor;

    FragColor = vec4((ambient + diffuse + specular) * objectColor, 1.0);
}
)");
    light_target_shader_phong.use();
    light_target_shader_phong.set_vec3("objectColor", { 1.f, 0.5f, 0.31f });
    light_target_shader_phong.set_vec3("lightColor", { 1.f, 1.f, 1.f });

    Shader light_target_shader_gouraud(
R"(#version 330 core
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aNormal;

out vec4 OurColor;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
uniform vec3 objectColor;
uniform vec3 lightColor;
uniform vec3 lightPos;

void main()
{
    gl_Position = projection * view * model * vec4(aPos, 1.0);
    vec3 FragPos = vec3(view * model * vec4(aPos, 1.0));
    vec3 Normal = mat3(transpose(inverse(view * model))) * aNormal;

    vec3 viewPos = vec3(0.0);
    vec3 lp = vec3(view * vec4(lightPos, 1.0));

    float ambientStrength = 0.1;
    vec3 ambient = ambientStrength * lightColor;

    vec3 lightDir = normalize(lp - FragPos);
    vec3 norm = normalize(Normal);
    float diff = max(dot(lightDir, norm), 0.0);
    vec3 diffuse = diff * lightColor;

    float specularStrength = 0.5;
    vec3 viewDir = normalize(viewPos - FragPos);
    vec3 reflectDir = reflect(-lightDir, norm);

    float spec = pow(max(dot(viewDir, reflectDir), 0.0), 32);
    vec3 specular = specularStrength * spec * lightColor;

    OurColor = vec4((ambient + diffuse + specular) * objectColor, 1.0);
}
)",
R"(#version 330 core
in vec4 OurColor;

out vec4 FragColor;

void main()
{
    FragColor = OurColor;
}
)");
    light_target_shader_gouraud.use();
    light_target_shader_gouraud.set_vec3("objectColor", { 1.f, 0.5f, 0.31f });
    light_target_shader_gouraud.set_vec3("lightColor", { 1.f, 1.f, 1.f });

    Shader light_source_shader(
R"(#version 330 core
layout (location = 0) in vec3 aPos;
layout (location = 0) in vec3 aNormal;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

void main()
{
    gl_Position = projection * view * model * vec4(aPos, 1.0);
}
)",
R"(#version 330 core
out vec4 FragColor;

void main()
{
    FragColor = vec4(1.0);
}
)");

    Camera camera = {};

    bool left = false;
    bool right = false;
    bool front = false;
    bool back = false;
    bool zoom = false;

    float timer = 0.f;

    glEnable(GL_DEPTH_TEST);

    auto pk = [&](AppEvent* e)
    {
        if (e->type == AppEventType_Keyupdown)
        {
            switch (e->key.code)
            {
            case AppKeyCode_A:
                left = e->key.is_down;
                break;
            case AppKeyCode_D:
                right = e->key.is_down;
                break;
            case AppKeyCode_W:
                front = e->key.is_down;
                break;
            case AppKeyCode_S:
                back = e->key.is_down;
                break;
            case AppKeyCode_LMouse:
                zoom = e->key.is_down;
                break;
            }
        }
    };

    auto update = [&](float dt, IntVec2 mouse_delta)
    {
        timer += dt;

        WindowSize window_size = win32_window_size(&win32);

        camera.rotate_dir(mouse_delta.x * dt, mouse_delta.y * dt);
        camera.update_move(left, right, front, back, dt);

        Mat4 projection = mat4_persp(camera.get_fov(zoom), (float)window_size.x / (float)window_size.y, 0.1f, 100.f);

        glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

        Vec3 light_pos = { 3.f * cos(timer), 3.f, 3.f * sin(timer) };

        light_target_shader_gouraud.use();
        light_target_shader_gouraud.set_mat4("model", mat4_scale(2, 2, 2));
        light_target_shader_gouraud.set_mat4("view", camera.get_view());
        light_target_shader_gouraud.set_mat4("projection", projection);
        light_target_shader_gouraud.set_vec3("lightPos", light_pos);
        //light_target_shader.set_vec3("viewPos", camera.pos);
        glBindVertexArray(light_target_vao);
        glDrawArrays(GL_TRIANGLES, 0, 36);

        Mat4 model = mat4_translate_vec3(light_pos);
        model = mat4_mult(model, mat4_scale(0.2f, 0.2f, 0.2f));
        light_source_shader.use();
        light_source_shader.set_mat4("model", model);
        light_source_shader.set_mat4("view", camera.get_view());
        light_source_shader.set_mat4("projection", projection);
        glBindVertexArray(light_source_vao);
        glDrawArrays(GL_TRIANGLES, 0, 36);
    };
    run_main(pk, update);
}

#endif
