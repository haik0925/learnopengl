#ifndef DEBUG_H
#define DEBUG_H
#include "primitive_types.h"
#ifdef _WIN32
#include <intrin.h>
#endif

#ifdef DEBUG
#define log_append(message, ...) debug_log(message, __VA_ARGS__)
#define log_(message, ...) debug_log_newline(message, __VA_ARGS__)
#define assert_(expression) do {if(!(expression)) __debugbreak(); } while(0)
#else
#define log_append(message, ...)
#define log_(message, ...)
#define assert_(expression) (expression)
#endif//DEBUG

#ifdef __cplusplus
extern "C" {
#endif

void debug_set_global_log_buffer(void* buffer, u64 size);
char* debug_get_global_log_buffer();
u64 debug_get_global_log_buffer_size();
void debug_log(const char* format, ...);
void debug_log_newline(const char* format, ...);
u64 debug_read_all_binary_file(const char* filename, void* out_buffer, u64 buffer_size);
void debug_write_all_binary_file(const char* filename, void* in_buffer, u64 buffer_size);

#ifdef __cplusplus
}
#endif

#endif//DEBUG_H
