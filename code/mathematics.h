#ifndef MATHEMATICS_H
#define MATHEMATICS_H

#ifndef __cplusplus
#include <stdbool.h>
#endif

#define math_pi32 3.14159265359f

typedef union Vec2
{
    struct
    {
        float x, y;
    };
    struct
    {
        float width, height;
    };
} Vec2;

typedef struct Vec3
{
    float x, y, z;
} Vec3;

typedef union Vec4
{
    struct
    {
        float x, y, z, w;
    };
    struct
    {
        float r, g, b, a;
    };
} Vec4;

typedef Vec4 Color;

typedef union IntVec2
{
    struct
    {
        int x, y;
    };
    struct
    {
        int width, height;
    };
} IntVec2;

typedef IntVec2 WindowSize;

typedef union IntVec3
{
    struct
    {
        int x, y, z;
    };
    struct
    {
        int width, height, length;
    };
} IntVec3;

// Row major
typedef struct Mat4
{
    float e[4][4];
} Mat4;

typedef struct
{
    union
    {
        Vec3 xyz;
        struct
        {
            float x, y, z;
        };
    };
    float w;
} Quaternion;

inline float to_radians(float degrees)
{
    float result = degrees * (math_pi32 / 180.f);
    return result;
}

inline float to_degrees(float radians)
{
    float result = radians * (180.f / math_pi32);
    return result;
}

#ifdef __cplusplus
extern "C" {
#endif

Vec2 vec2(float x, float y);
Vec2 vec2_negate(Vec2 v);
Vec2 vec2_add(Vec2 a, Vec2 b);
Vec2 vec2_sub(Vec2 a, Vec2 b);
Vec2 vec2_mult(Vec2 a, Vec2 b);
Vec2 vec2_multf(Vec2 a, float b);
Vec2 vec2_div(Vec2 a, Vec2 b);
Vec2 vec2_divf(Vec2 a, float b);
float vec2_dot(Vec2 a, Vec2 b);
bool vec2_equals(Vec2 a, Vec2 b);
float vec2_lengthsq(Vec2 v);
float vec2_length(Vec2 v);
Vec2 vec2_normalize(Vec2 v);

Vec3 vec3(float x, float y, float z);
Vec2 vec3_xy(Vec3 v);
Vec2 vec3_xz(Vec3 v);
Vec2 vec3_yz(Vec3 v);
Vec3 vec3_negate(Vec3 v);
Vec3 vec3_add(Vec3 a, Vec3 b);
Vec3 vec3_sub(Vec3 a, Vec3 b);
Vec3 vec3_mult(Vec3 a, Vec3 b);
Vec3 vec3_multf(Vec3 a, float b);
Vec3 vec3_div(Vec3 a, Vec3 b);
Vec3 vec3_divf(Vec3 a, float b);
float vec3_dot(Vec3 a, Vec3 b);
Vec3 vec3_cross(Vec3 a, Vec3 b);
bool vec3_equals(Vec3 a, Vec3 b);
float vec3_lengthsq(Vec3 v);
float vec3_length(Vec3 v);
Vec3 vec3_normalize(Vec3 v);

Vec4 vec4(float x, float y, float z, float w);
Vec3 vec4_xyz(Vec4 v);
Vec4 vec4_multf(Vec4 a, float b);

Color color_red();
Color color_blue();
Color color_green();
Color color_white();
Color color_black();

IntVec2 intvec2(int x, int y);
IntVec2 intvec2_sub(IntVec2 a, IntVec2 b);

Mat4 mat4(
    float e00, float e01, float e02, float e03,
    float e10, float e11, float e12, float e13,
    float e20, float e21, float e22, float e23,
    float e30, float e31, float e32, float e33);
Mat4 mat4_diagonal(float v);
Mat4 mat4_identity();
Mat4 mat4_transpose(Mat4 m);
Mat4 mat4_negate(Mat4 m);
Mat4 mat4_add(Mat4 a, Mat4 b);
Mat4 mat4_sub(Mat4 a, Mat4 b);
Mat4 mat4_mult(Mat4 a, Mat4 b);
Mat4 mat4_multf(Mat4 a, float b);
Vec4 mat4_mult_vec4(Mat4 a, Vec4 b);

Mat4 mat4_ortho(float left, float right, float bottom, float top, float nearZ, float farZ);
Mat4 mat4_persp(float fov, float aspect_ratio, float near_z, float far_z);
Mat4 mat4_translate(float x, float y, float z);
Mat4 mat4_translate_vec3(Vec3 t);
Mat4 mat4_scale(float x, float y, float z);
Mat4 mat4_scale_vec3(Vec3 s);
bool mat4_inverse(Mat4 in, Mat4* out);
Mat4 mat4_rotate(float angle, Vec3 axis);
Mat4 mat4_lookat(Vec3 eye, Vec3 target, Vec3 up);

inline Quaternion quaternion(float x, float y, float z, float w);
inline Quaternion quaternion_vec4(Vec4 v);
inline Quaternion quaternion_add(Quaternion a, Quaternion b);
inline Quaternion quaternion_sub(Quaternion a, Quaternion b);

// origin of screen is left botton
Vec3 screen_to_world(IntVec2 screen_pos, IntVec2 screen_size, Mat4 proj, Mat4 view);
Vec3 screen_to_world_ray(IntVec2 screen_pos, IntVec2 screen_size, Vec3 camera_pos, Mat4 proj, Mat4 view);

#ifdef __cplusplus
}
#endif

#endif//MATHEMATICS_H
