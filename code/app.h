#ifndef APP_H
#define APP_H

typedef enum
{
    AppKeyCode_Undefined = 0,
    AppKeyCode_LMouse,
    AppKeyCode_RMouse,
    AppKeyCode_ESC,
    AppKeyCode_Space,
    AppKeyCode_0,
    AppKeyCode_1,
    AppKeyCode_2,
    AppKeyCode_3,
    AppKeyCode_4,
    AppKeyCode_5,
    AppKeyCode_6,
    AppKeyCode_7,
    AppKeyCode_8,
    AppKeyCode_9,
    AppKeyCode_A,
    AppKeyCode_D,
    AppKeyCode_R,
    AppKeyCode_S,
    AppKeyCode_W,
    AppKeyCode_Y,
    AppKeyCode_Z,
    AppKeyCode_Shift,
    AppKeyCode_Control,
    AppKeyCode_F1,
    AppKeyCode_F2,
    AppKeyCode_F3,
    AppKeyCode_F4,
    AppKeyCode_F5,
    AppKeyCode_F6,
    AppKeyCode_F7,
    AppKeyCode_F8,
    AppKeyCode_F9,
    AppKeyCode_F10,
    AppKeyCode_F11,
    AppKeyCode_F12,
    AppKeyCode_Delete,
} AppKeyCode;

typedef enum
{
    AppEventType_Undefined = 0,
    AppEventType_Quit,
    AppEventType_Keyupdown,
} AppEventType;

typedef struct
{
    AppEventType type;
    AppKeyCode code;
    bool is_down;
    bool alt_is_down;
} AppKeyEvent;

typedef union AppEvent
{
    AppEventType type;
    AppKeyEvent key;
} AppEvent;

#endif//APP_H
