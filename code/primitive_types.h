#ifndef PRIMITIVE_TYPES_H
#define PRIMITIVE_TYPES_H

#include <stdbool.h>
#include <inttypes.h>

typedef unsigned int uint;

typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

typedef int8_t i8;
typedef int16_t i16;
typedef int32_t i32;
typedef int64_t i64;

// Use these for opaque types
typedef u64 Handle64;
typedef u64 Handle32;

#endif//PRIMITIVE_TYPES_H
