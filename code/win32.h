#ifndef WIN32_H
#define WIN32_H

#include "primitive_types.h"
#include "mathematics.h"

typedef union AppEvent AppEvent;

typedef u64 Win32Clock; 

typedef struct
{
    Handle64 instance;
    Handle64 window;
    Handle64 dc;
} Win32Context;

#ifdef __cplusplus
extern "C" {
#endif

bool win32_poll_event(AppEvent* out_event);
void win32_init_wgl(Win32Context* win32);
Win32Context win32_init(int window_width, int window_height);
void win32_show_cursor(bool show);
WindowSize win32_window_size(const Win32Context* ctx);
float win32_elapsed_time(Win32Clock start, Win32Clock end);
Win32Clock win32_current_clock();
void win32_cursor_pos(const Win32Context* ctx, int* out_x, int* out_y);
void win32_set_cursor_pos(const Win32Context* ctx, int x, int y);
bool win32_is_window_focused();

#ifdef __cplusplus
}
#endif

#endif//WIN32_H
